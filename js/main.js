
listSV = []
var ShowData = localStorage.getItem("DATA")
if (ShowData!==null) {
    listSV = JSON.parse(ShowData).map(function(item) {
        return new Sinhvien(item.ma,item.ten,item.email,item.pass,item.toan,item.ly,item.hoa)   
    })
   
    redenListSV(listSV)
}
// save local
function saveLocalrange () {
    redenListSV(listSV)
    var data = JSON.stringify(listSV)
    localStorage.setItem("DATA",data)

}
function themSinhVien() {
    var sv =  layThongtinTuForm()  
    listSV.push(sv)
    saveLocalrange() 
    document.querySelector("#formQLSV").reset()   
}

function XoaSinhVien(ma) {
    var index = listSV.findIndex(function(item) {
        return item.ma == ma
    })
    listSV.splice(index,1)
    saveLocalrange()
  
    
}
function SuaSinhvien(ma) {
   var index = listSV.findIndex(function(item) {
        return item.ma == ma
    })
    document.getElementById("txtMaSV").disabled = true

     ShowThongtinlenform(listSV[index])
    
    
   
}
function capNhatSinhVien() {
        sv = layThongtinTuForm()
        var index = listSV.findIndex(function(item) {
            return item.ma == sv.ma
        })
        listSV[index] = layThongtinTuForm()
        saveLocalrange()
        document.getElementById("txtMaSV").disabled = false
        document.querySelector("#formQLSV").reset() 

}
var search = document.getElementById("txtSearch")
search.onkeyup = function timKiemSinhVienTheoTen() {
    var renderSvTheoName = []
    var input = search.value
    if (input.length>0) {
        listSV.forEach(function(sv) {
            var ten = sv.ten
            if (ten.includes(input)) {
                renderSvTheoName.push(sv)
                redenListSV(renderSvTheoName)
            } 
        })
    }
    else {
        redenListSV(listSV)
   
    }
}
function Reset(){
    listSV = []
    redenListSV(listSV)
    saveLocalrange()
}